# TCS3472 COLOR LIGHT-TO-DIGITAL CONVERTER with IR FILTER

## General description of the sensor

* This sensor provides a digital return of red, green, blue (RGB), and clear light values. 
* IR filter integrated that minimizes the IR spectral component of the incoming light and allows more accurate measurements.

*	4 ADC’s which converts the photodiodes' current to a 16-bit digital value. These values are stored later in the data registers.

*	Communications with the sensor over a I2C serial bus connection up to 400Khz fast.

*	Interrupts. The sensor provides a interrupt signal output which simplifies and improves the efficiency of the system software by eliminating the need of a continous pulling of the data from the sensor. An upper and lower threshold can be defined.

* Interrupt persistence filter: allows to define a number of consecutive threshold matches before generating the interrupt signal. 

* Use cases: heatlh/fitness products, industrial process controls, medical diagnostic equipment, etc.



![alt text][sensor]



## System states diagram

We can differentiate 4 states:

*	Sleep: low-power consumption.
*	Idle: Intermediate state.
*	Wait: The sensor waits WLONG tine and then go to RGBC state.
    * WTIME: wait time.
    * WLONG: time x12.
*	RGBC: Measure state.


![alt text][time-diagram]


## Registers of interest

### Command register

Specifies the address of the target register for future write and read operations.The command byte contains either control information or a 5-bit register address. The control
commands can also be used to clear interrupts.


* CMD [7]: Must write as 1 when addressing COMMAND register.
* TYPE [6:5]: Selects type of transaction to follow in subsequent data transfers
    * 00:  Repeated byte protocol transaction
    * 01:  Auto-incremented protocol transaction (blocks). 
    * 10:  Reserved 
    * 11:  Special function. Need to set 00110 in ADDR field to clear channel interrupt.
* ADDR/SF [4:0]: Depending on the value of “TYPE”, a register address is written, or the special function (clear interrupt) is used.


![alt text][command-register]

### Enable Register (0x00)


Used primarily to power the sensor on and off, and enable functions and interrupts


![alt text][enable-register]



### Timing Register (0x01)



To control the internal integration time of the RGBC clear and IR channel ADCs in 2.4-ms increments

```
Max RGBC Count = (256 − ATIME) × 1024 up to a maximum of 65535
```


![alt text][timing-register]



### Control Register (0x0F)


To set the gain of the four ADCs for the RGBC photodiodes.


![alt text][control-register]


### Status Register (0x13)


Read only register. Provides information about the internal status of the device.


![alt text][status-register]



### RGBC Channel Data Registers (0x14 − 0x1B)




* Read only registers. 
* Data is stored as a 16-bit values.
* To read a measure, two-byte I2C transaction are needed
    * One for the 8-bit lower part
    * Another for the 8-bit upper part 


![alt text][rgbc-register]


## Example


Check out the example code  [here](./example/main.cpp)


Create a variable for the I2C connection and save the colour sensor address:

```
I2C i2c(D14,D15); //pins for I2C communication (SDA, SCL)

//Address of the sensor 0x29 (page 3 from datasheet)
int sensor_addr = 41 << 1;
```

Inside the ```main()``` function set up the I2C connection frequency to 400khz (the maximum for this sensor):



```
i2c.frequency(400000);
```

In order to check if the sensor is the TCS3472 model, a call to the device ID can be made:

``` 
//send 0b10010010 
char id_regval[1] = {146}; //0x92 -> 0x12 ID Device register
    

char data[1] = {0}; //Char array to store results
i2c.write(sensor_addr,id_regval,1, true);
i2c.read(sensor_addr,data,1,false);
    
//If returned 0x44 (hexadecimal) connected to the TCS34725 sensor 
if (data[0]==68) {
    pc.printf("Successfully connected to the TCS34725 sensor");
}else {
    pc.printf("Not connected to the TCS34725 sensor")  
}
```

After checking of the connected sensor is the right model it's time to set up the initial configuration:

+ Set the configuration for the RGBC controller to 256 the integration cycle (700ms)

```
char timing_register[2] = {129,0}; //0x81 -> 0x01 RGBC Timing Register
i2c.write(sensor_addr,timing_register,2,false);
```

+ Establish the RGBC gain value to 1

```
char control_register[2] = {143,0}; //0x8F -> 0x0F Control register
i2c.write(sensor_addr,control_register,2,false);
```

+ Enable the states and interrupts. Sends 011 to the register to set the AEN and PON bits -> state RGBC

```
char enable_register[2] = {128,3}; //0x80 -> 0x00 Enable register
i2c.write(sensor_addr,enable_register,2,false);
```



Finally, the colour measures can be read from the registers explained above like this:

```  
char red_reg[1] = {150};//0x96 -> 0x16 RDATAL register
char red_data[2] = {0,0};
i2c.write(sensor_addr,red_reg,1, true);
i2c.read(sensor_addr,red_data,2, false);      

int red_value = ((int)red_data[1] << 8) | red_data[0];
```


[states-diagram]: resources/states-diagram.png "Colour Sensor States Diagram"
[sensor]: resources/sensor.png "Colour sensor diagram"
[command-register]: resources/command-register.png "Command register"
[control-register]: resources/control-register.png "Control register"
[enable-register]: resources/enable-register.png "Enable register"
[rgbc-register]: resources/rgbc-register.png "RGBC register"
[timing-register]: resources/timing-register.png "Timing register"
[time-diagram]: resources/time-diagram.jpeg "States diagram"
[status-register]: resources/status-register.png "Timing register"

[example]: example/main.cpp "Example code"

