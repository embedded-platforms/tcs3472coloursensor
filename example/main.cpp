// Example program connecting to the TCS34725 Color Sensor to the K64F using I2C

#include "mbed.h"

I2C i2c(D14,D15); //pins for I2C communication (SDA, SCL)
Serial pc(USBTX, USBRX,9600);

//Address of the sensor 0x29 (page 3 from datasheet)
int sensor_addr = 41 << 1;

DigitalOut led(LED4);

int main() {
    led = 0; // off
    
    // Connect to the Color sensor and verify whether we connected to the correct sensor. 
    
    i2c.frequency(400000);
    
    //send 0b10010010 
    char id_regval[1] = {146}; //0x92. Command Register address 0x12 (ID Device)
    pc.printf("\n\rConnecting to the sensor");
    
    //Char array to store results
    char data[1] = {0};
    i2c.write(sensor_addr,id_regval,1, true);
    i2c.read(sensor_addr,data,1,false);
    
    //If returned 0x44 (hexadecimal) connected to the TCS34725 sensor 
    if (data[0]==68) {
        pc.printf("\n\rSuccessfully connected to the TCS34725 sensor");
        led = 0;
        wait (2); 
        led = 1;
        } else {
        led = 1; 
    }
    
    // Initialize color sensor
    
    //Set the configuration for the RGBC controller to 256 the integration cycle(700ms)
    char timing_register[2] = {129,0}; //0x81 RGBC Timing Register
    i2c.write(sensor_addr,timing_register,2,false);
    
    //Call to the CONTROL register 0x0F: stablish the RGBC gain value to 1
    char control_register[2] = {143,0}; //0x8F
    i2c.write(sensor_addr,control_register,2,false);
    
    //Call to enable the states and interrupts. Sends 011 to the register to acive the AEN and PON and it's state now is RGBC
    char enable_register[2] = {128,3}; //{0x80, 0x03}
    i2c.write(sensor_addr,enable_register,2,false);
    
    // Read data from color sensor (Clear/Red/Green/Blue)
    
    while (true) { 
        char clear_reg[1] = {148};//Register 0x14 CDATAL
        char clear_data[2] = {0,0};
        i2c.write(sensor_addr,clear_reg,1, true);
        i2c.read(sensor_addr,clear_data,2, false); //Read high y lower byte
        //Store both bytes in the variable clear_value
        int clear_value = ((int)clear_data[1] << 8) | clear_data[0];
        
        char red_reg[1] = {150};//Register 0x16 RDATAL
        char red_data[2] = {0,0};
        i2c.write(sensor_addr,red_reg,1, true);
        i2c.read(sensor_addr,red_data,2, false);
        
        int red_value = ((int)red_data[1] << 8) | red_data[0];
        
        char green_reg[1] = {152};
        char green_data[2] = {0,0};
        i2c.write(sensor_addr,green_reg,1, true);
        i2c.read(sensor_addr,green_data,2, false);
        
        int green_value = ((int)green_data[1] << 8) | green_data[0];
        
        char blue_reg[1] = {154};
        char blue_data[2] = {0,0};
        i2c.write(sensor_addr,blue_reg,1, true);
        i2c.read(sensor_addr,blue_data,2, false);
        
        int blue_value = ((int)blue_data[1] << 8) | blue_data[0];
        
        // print sensor readings
        
        pc.printf("\n\rClear (%d), Red (%d), Green (%d), Blue (%d)\n", clear_value, red_value, green_value, blue_value);
        wait(0.5);
    }
    
}
